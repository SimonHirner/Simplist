package com.simplist.simplist;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.simplist.simplist.R.layout;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Menü-Ansicht für Auswahl der Kategorie.
 */
public class MenuActivity extends AppCompatActivity {
    // Position der aktuellen Kategorie
    public static int currentCategoryPosition;
    // Kategorieliste beinhaltet Kategorien
    public static ArrayList<String> categoryList;
    // Bezeichnung der aktuellen Kategorie
    public static String currentCategory;
    // ListView beinhaltet ArrayAdapter
    ListView listViewCategoryList;
    // Custom ArrayAdapter beinhaltet Kategorieliste
    CustomCategorieView arrayAdapter;
    // Temporäre Kategorieliste, um mehrere Kategorien gleichzeitig zu markieren
    ArrayList<String> categoryListItems;
    // Zählt die markierten Kategorien
    int counter;

    /**
     * Wird aufgerufen, wenn die Aktivität gestartet wird.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Standardbefehle
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        // GUI-Elemente initialisieren
        listViewCategoryList = findViewById(R.id.listViewCategorieList);

        // SharedPrefs laden
        loadData();

        // Footer mit Button für Taskliste erstellen
        LayoutInflater inflater = getLayoutInflater();
        View footer = inflater.inflate(R.layout.custom_categorie_view_footer, listViewCategoryList, false);
        listViewCategoryList.addFooterView(footer, null, false);
        final TextView textViewAddCategory = footer.findViewById(R.id.textViewAddCategory);

        // Custom ArrayAdapter initialisieren
        arrayAdapter = new CustomCategorieView(this, categoryList);

        // ArrayAdapter mit ListView verknüpfen
        listViewCategoryList.setAdapter(arrayAdapter);

        // Initialisierung der temporären Kategorieliste
        categoryListItems = new ArrayList<String>();
        // Mehrfachauswahl bei Kategorieliste erlauben
        listViewCategoryList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        // Mehrfachauswahl Click Listener
        listViewCategoryList.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
             @Override
             public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                 // Ausgewählte Kategorie in temporäre Liste speichern
                 categoryListItems.add(categoryList.get(position));
                 // Counter hochzählen
                 counter++;
                 // Letzte Markierung speichern
                 currentCategory = categoryList.get(position);
             }

             @SuppressLint("ResourceType")
             @Override
             public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                 counter = 0;
                 // Menü erstellen
                 MenuInflater inflater1 = mode.getMenuInflater();
                 inflater1.inflate(R.layout.category_context_menu, menu);
                 return true;
             }

             @Override
             public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                 return false;
             }

             @Override
             public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                 // Ausgewählte Kategorien löschen
                 switch (item.getItemId()) {
                     case R.id.itemDeleteCategory:
                         for (String msg : categoryListItems) {
                             categoryList.remove(msg);
                             // ArrayAdapter aktualisieren
                             arrayAdapter.notifyDataSetChanged();
                             // Änderungen in SharedPrefs speichern
                             saveData();
                         }
                         mode.finish();
                         return true;
                     case R.id.itemChangeCategory:
                         if (counter == 1) {
                             // Zur Category Detail Activity wechseln
                             Intent intent = new Intent(MenuActivity.this, CategoryDetailActivity.class);
                             intent.putExtra("textCategoryName", currentCategory);
                             startActivity(intent);
                             mode.finish();
                             return true;
                         }
                         mode.finish();
                         return true;
                     default:
                         return false;
                 }
             }

             @Override
             public void onDestroyActionMode(ActionMode mode) {

             }
         });

        // OnClickListener für ListView
        listViewCategoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                // Intent für TaskDetailActivity erstellen
                Intent intent = new Intent(MenuActivity.this, MainActivity.class);
                // Position der Kategorie speichern
                currentCategoryPosition = position;
                currentCategory = categoryList.get(position);
                // Aktivität wechseln
                startActivity(intent);
            }
        });

        // OnClickListener für TextViewAddCategory
        textViewAddCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Zur AddCategoryActivity springen
                Intent intent = new Intent(MenuActivity.this, AddCategoryActivity.class);
                startActivity(intent);
            }
        });

    }

    /**
     * Wird aufgerufen, wenn zurück zur Aktivität gewechselt wird.
     */
    @Override
    protected void onResume() {
        // Standardbefehl
        super.onResume();

        // Intent speichern
        Intent intent = getIntent();

        // Prüfen, ob shouldAddCategory gesetzt ist
        if (intent.getBooleanExtra("shouldAddCategory", false)) {
            // shouldAdd auf false setzen
            intent.putExtra("shouldAddCategory", false);
            // Neue Kategorie erstellen
            String newCategory = intent.getStringExtra("newCategory");
            // Neuen Task der Taskliste hinzufügen
            categoryList.add(newCategory);
        }

        // Prüfen, ob shouldChangeCategory gesetzt ist
        if (intent.getBooleanExtra("shouldChangeCategory", false)) {
            // shouldDelCategory auf false setzen
            intent.putExtra("shouldChangeCategory", false);
            // Kategorie übergeben
            currentCategory = intent.getStringExtra("newCategoryName");
            // Kategorieliste durchgehen
            for (int i = 0; i < categoryList.size(); i++) {
                // Prüfen, ob Kategoriename übereinstimmt
                if (intent.getStringExtra("oldCategoryName").equals(categoryList.get(i))) {
                    // Kategorie ändern
                    categoryList.set(i, currentCategory);
                }
            }
        }

        // Prüfen, ob shouldDelCategory gesetzt ist
        if (intent.getBooleanExtra("shouldDelCategory", false)) {
            // shouldDelCategory auf false setzen
            intent.putExtra("shouldDelCategory", false);
            // Kategorie übergeben
            currentCategory = intent.getStringExtra("delCategory");
            // Kategorie löschen
            categoryList.remove(currentCategory);
        }

        // ArrayAdapter aktualisieren
        arrayAdapter.notifyDataSetChanged();
        // Änderungen in SharedPrefs speichern
        saveData();
    }

    /**
     * Wird aufgerufen, wenn Aktivität endgültig beendet wird.
     */
    @Override
    protected void onDestroy() {
        // Standardbefehl
        super.onDestroy();
        // Daten nochmal in SharedPrefs speichern (eigentlich überflüssig)
        saveData();
    }

    /**
     * Tasklist und Closed Tasklist in SharedPref speichern.
     */
    public void saveData() {
        SharedPreferences sharedPreferencesCat = getSharedPreferences("shared_preferencesCat", MODE_PRIVATE);
        SharedPreferences.Editor editorCat = sharedPreferencesCat.edit();
        Gson gsonCat = new Gson();
        String jsonCat = gsonCat.toJson(categoryList);
        editorCat.putString("tasklistCat", jsonCat);
        editorCat.apply();
    }

    /**
     * Tasklist und Closed Tasklist aus SharedPref laden.
     */
    public void loadData() {
        SharedPreferences sharedPreferencesCat = getSharedPreferences("shared_preferencesCat", MODE_PRIVATE);
        Gson gsonCat = new Gson();
        String jsonCat = sharedPreferencesCat.getString("tasklistCat", null);
        Type typeCat = new TypeToken<ArrayList<String>>() {}.getType();
        categoryList = gsonCat.fromJson(jsonCat, typeCat);
        if (categoryList == null) {
            categoryList = new ArrayList<>();
        }
    }

    /**
     * Tasklist und Closed Tasklist in SharedPref löschen (zum Zurücksetzen).
     */
    public void deleteData() {
        SharedPreferences sharedPreferencesCat = getSharedPreferences("shared_preferencesCat", MODE_PRIVATE);
        SharedPreferences.Editor editorCat = sharedPreferencesCat.edit();
        editorCat.clear();
        editorCat.apply();
    }
}
