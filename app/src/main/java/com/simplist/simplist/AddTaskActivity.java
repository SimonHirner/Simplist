package com.simplist.simplist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Aktivität, um einen neuen Task hinzuzufügen.
 */
public class AddTaskActivity extends AppCompatActivity {
    // Button
    Button buttonAddTask;
    // Text Views
    TextView editTextTaskName;
    TextView editTextTaskDescription;
    // Spinner
    Spinner spinnerCategory;
    // Variablen für Textfelder
    String textTaskName;
    String textTaskDescription;
    // Variable für Spinner
    String textCategory;
    // Kategorieliste beinhaltet alle Kategorien
    ArrayList<String> categoryList;

    /**
     * Wird aufgerufen, wenn die Aktivität gestartet wird.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Standardbefehle
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        // GUI-Elemente verknüpfen
        buttonAddTask = findViewById(R.id.buttonAddTask);
        editTextTaskName = findViewById(R.id.editTextTaskName);
        editTextTaskDescription = findViewById(R.id.editTextTaskDescription);
        spinnerCategory = findViewById(R.id.spinnerCategory);

        // Kategorieliste abspeichern
        categoryList = MenuActivity.categoryList;

        // Spinner mit aktueller Kategorieliste befüllen
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, categoryList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerCategory.setAdapter(adapter);

        // OnClickListener für den AddTaskButton
        buttonAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Werte der Textfelder in Variablen speichern
                textTaskName = editTextTaskName.getText().toString();
                textTaskDescription = editTextTaskDescription.getText().toString();
                textCategory = spinnerCategory.getSelectedItem().toString();
                // Neuer Intent zur MainActivity
                Intent intent = new Intent(AddTaskActivity.this, MainActivity.class);
                // Neuen Task erstellen
                Task newTask = new Task(textTaskName, textTaskDescription, textCategory);
                // shouldAdd auf true setzen
                intent.putExtra("shouldAdd", true);
                // Neuen Task übergeben
                intent.putExtra("newTask", newTask);
                // Zurück zur MainActivity wechseln
                startActivity(intent);
            }

        });
    }


}
