package com.simplist.simplist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

/**
 * Klasse für einen custom Array Adapter, welcher die Kategorieliste beinhaltet.
 */
public class CustomCategorieView extends ArrayAdapter<String> {
    // Taskliste beinhaltet Tasks
    private ArrayList<String> categorielist;
    // Beinhaltet Kontext
    private Activity context;

    /**
     * Konstruktor.
     *
     * @param context
     * @param data
     */
    public CustomCategorieView(@NonNull Activity context, ArrayList<String> data) {
        super(context, R.layout.custom_categorie_view, data);
        this.categorielist = data;
        this.context = context;
    }

    /**
     * Muss implementiert werden.
     *
     * @param position
     * @param view
     * @param parent
     * @return
     */
    public View getView(final int position, View view, ViewGroup parent) {
        // Layout Inflater
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.custom_categorie_view, null);

        // Position des aktuellen Task herausfinden
        String categorie = categorielist.get(position);

        // GUI-Elemente initialisieren
        TextView textViewCategorie = rowView.findViewById(R.id.textViewCategorie);

        // GUI-Elemente befüllen
        textViewCategorie.setText(categorie);

        return rowView;
    }
}
