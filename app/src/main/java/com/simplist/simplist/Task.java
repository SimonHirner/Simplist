package com.simplist.simplist;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.UUID;

/**
 * Klasse beschreibt einen Task.
 * Implementiert Parcelable, um Objekte zwischen Aktivitäten herumzureichen.
 *
 */
class Task implements Parcelable {

    // Name des Task
    private String taskName;
    // Beschreibung des Task
    private String taskDescription;
    // Status des Task
    private boolean taskState;
    // Einmalige ID des Task
    private String uniqueID;
    // Kategorie des Task
    private String category;

    // Getter & Setter
    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public boolean getTaskState() {
        return taskState;
    }

    public void setTaskState(boolean taskState) {
        this.taskState = taskState;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * Keine Funktion, muss implementiert werden.
     *
     * @return 0
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Muss implementiert werden.
     *
     * @param dest
     * @param flags
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(taskName);
        dest.writeString(taskDescription);
        dest.writeByte((byte) (taskState ? 1 : 0));
        dest.writeString(uniqueID);
        dest.writeString(category);
    }

    /**
     * Konstruktor für Parcelable.
     *
     * @param in
     */
    protected Task(Parcel in) {
        taskName = in.readString();
        taskDescription = in.readString();
        taskState = in.readByte() != 0;
        uniqueID = in.readString();
        category = in.readString();
    }

    /**
     * Muss implementiert werden.
     *
     */
    public static final Creator<Task> CREATOR = new Creator<Task>() {
        /**
         * Muss implementiert werden.
         *
         * @param in
         * @return
         */
        @Override
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        /**
         * Muss implementiert werden.
         *
         * @param size
         * @return
         */
        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    /**
     * Konstruktor. Erstellt eine eindeutige ID für jeden Task. Setzt den Status auf false.
     *
     * @param taskName
     * @param taskDescription
     */
    public Task(String taskName, String taskDescription, String category) {
        this.taskName = taskName;
        this.taskDescription = taskDescription;
        this.taskState = false;
        this.uniqueID = UUID.randomUUID().toString() + String.valueOf(System.currentTimeMillis());
        this.category = category;
    }

    /**
     * ToString.
     *
     * @return
     */
    @Override
    public String toString() {
        return taskName + " *** " + category;
    }

    /**
     * Methode, um alle Attribute (außer ID) eines Tasks zu ändern.
     *
     * @param taskName
     * @param taskDescription
     */
    public void changeTask(String taskName, String taskDescription, String category) {
        this.taskName = taskName;
        this.taskDescription = taskDescription;
        this.category = category;
    }
}

