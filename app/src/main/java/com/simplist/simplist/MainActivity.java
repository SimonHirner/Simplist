package com.simplist.simplist;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;


import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    // Taskliste beinhaltet Tasks
    ArrayList<Task> tasklist;
    // ListView beinhaltet ArrayAdapter
    ListView listViewTasklist;
    // Custom ArrayAdapter beinhaltet Taskliste und Checkboxen
    CustomListView arrayAdapter;
    // Aktuelle Task ID
    String currentTaskID;

    // Closed Taskliste beinhaltet geschlossene Tasks
    ArrayList<Task> closedTasklist;
    // ListView beinhaltet ArrayAdapter für closed Taskliste
    ListView listViewClosedTasklist;
    // Custom ArrayAdapter für closed Taskliste beinhaltet Taskliste und Checkboxen
    CustomListView arrayAdapterClosed;

    // Temporäre Taskliste, um mehrere Kategorien gleichzeitig zu markieren
    ArrayList<Task> tasklistItems;
    // Zählt die markierten Tasks
    int counter;
    // Aktueller Task
    Task currentTask;


    /**
     * Wird aufgerufen, wenn die Aktivität gestartet wird.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Standardbefehle
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // GUI-Elemente initialisieren
        listViewTasklist = findViewById(R.id.listViewTasklist);
        listViewClosedTasklist = findViewById(R.id.listViewClosedTasklist);
        FloatingActionButton fabButtonAddTask = findViewById(R.id.fabButtonAddTask);


        // SharedPrefs laden
        loadData();

        // Closed Taskliste verbergen
        listViewClosedTasklist.setVisibility(View.GONE);

        // Footer mit Button für Taskliste erstellen
        LayoutInflater inflater = getLayoutInflater();
        View footer = inflater.inflate(R.layout.custom_list_view_footer, listViewTasklist, false);
        listViewTasklist.addFooterView(footer, null, false);
        final Button buttonShowHide = footer.findViewById(R.id.buttonShowHide);

        // Custom ArrayAdapter initialisieren
        arrayAdapter = new CustomListView(this, tasklist, this, true);
        arrayAdapterClosed = new CustomListView(this, closedTasklist, this, false);

        // ArrayAdapter mit ListView verknüpfen
        listViewTasklist.setAdapter(arrayAdapter);
        listViewClosedTasklist.setAdapter(arrayAdapterClosed);

        // OnClickListener für FAButtonAddTask
        fabButtonAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Zur AddTaskActivity springen
                Intent intent = new Intent(MainActivity.this, AddTaskActivity.class);
                startActivity(intent);
            }
        });

        // OnClickListener für ListView
        listViewTasklist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                // Aktuellen Task herausfinden
                Task currentTask = (Task) listViewTasklist.getItemAtPosition(position);
                // ID abspeichern
                currentTaskID = currentTask.getUniqueID();
                // Intent für TaskDetailActivity erstellen
                Intent intent = new Intent(MainActivity.this, TaskDetailActivity.class);
                // ID übergeben
                intent.putExtra("currentTaskID", currentTask.getUniqueID());
                intent.putExtra("textTaskName", currentTask.getTaskName());
                intent.putExtra("textTaskDescription", currentTask.getTaskDescription());
                intent.putExtra("textCategory", currentTask.getCategory());
                // Aktivität wechseln
                startActivity(intent);
            }
        });

        // OnClickListener für ButtonShowHide
        buttonShowHide.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Änderung der Sichtbarkeit der closed Taskliste und der Buttonbeschriftung
                    if (listViewClosedTasklist.getVisibility() == View.VISIBLE) {
                        listViewClosedTasklist.setVisibility(View.GONE);
                        buttonShowHide.setText(R.string.show);
                    } else {
                        listViewClosedTasklist.setVisibility(View.VISIBLE);
                        buttonShowHide.setText(R.string.hide);
                    }
                }
        });

        // Initialisierung der temporären Kategorieliste
        tasklistItems = new ArrayList<Task>();
        // Mehrfachauswahl bei Kategorieliste erlauben
        listViewTasklist.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        // Mehrfachauswahl Click Listener
        listViewTasklist.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                // Ausgewählten Task in temporäre Liste speichern
                tasklistItems.add(tasklist.get(position));
                // Counter hochzählen
                counter++;
                // Letzte Markierung speichern
                currentTask = tasklist.get(position);
            }

            @SuppressLint("ResourceType")
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                counter = 0;
                // Menü erstellen
                MenuInflater inflater1 = mode.getMenuInflater();
                inflater1.inflate(R.layout.category_context_menu, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                // Ausgewählten Task löschen
                switch (item.getItemId()) {
                    case R.id.itemDeleteCategory:
                        for (int i = 0; i < tasklistItems.size(); i++) {
                            for (int n = 0; n < tasklist.size(); n++) {
                                // Prüfen, ob ID übereinstimmt
                                if (tasklistItems.get(i).getUniqueID().equals(tasklist.get(n).getUniqueID())) {
                                    // Task entfernen
                                    tasklist.remove(n);
                                    // ArrayAdapter aktualisieren
                                    arrayAdapter.notifyDataSetChanged();
                                    // Änderungen in SharedPrefs speichern
                                    saveData();
                                }
                            }
                        }
                        mode.finish();
                        return true;
                    case R.id.itemChangeCategory:
                        if (counter == 1) {
                            // Zur Task Detail Activity wechseln
                            Intent intent = new Intent(MainActivity.this, TaskDetailActivity.class);
                            intent.putExtra("currentTaskID", currentTask.getUniqueID());
                            intent.putExtra("textTaskName", currentTask.getTaskName());
                            intent.putExtra("textTaskDescription", currentTask.getTaskDescription());
                            intent.putExtra("textCategory", currentTask.getCategory());
                            startActivity(intent);
                            mode.finish();
                            return true;
                        }
                        mode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });


    }

    /**
     * Wird aufgerufen, wenn zur Aktivität zurückgegangen wird.
     */
    @Override
    protected void onResume() {
        // Standardbefehl
        super.onResume();

        // Intent speichern
        Intent intent = getIntent();

        // Prüfen, ob shouldAdd gesetzt ist
        if (intent.getBooleanExtra("shouldAdd", false)) {
            // shouldAdd auf false setzen
            intent.putExtra("shouldAdd", false);
            // Neuen Task erstellen
            Task newTask = intent.getParcelableExtra("newTask");
            // Neuen Task der Taskliste hinzufügen
            tasklist.add(newTask);
        }

        // Prüfen, ob should Del gesetzt ist
        if (intent.getBooleanExtra("shouldDel", false)) {
            // shouldDel auf false setzen
            intent.putExtra("shouldDel", false);
            // Task ID übergeben
            currentTaskID = intent.getStringExtra("currentTaskID");
            // Taskliste durchgehen
            for (int i = 0; i < tasklist.size(); i++) {
                // Prüfen, ob ID übereinstimmt
                if (currentTaskID.equals(tasklist.get(i).getUniqueID())) {
                    // Task entfernen
                    tasklist.remove(i);
                }
            }
        }

        // Prüfen, ob should Change gesetzt ist
        if (intent.getBooleanExtra("shouldChange", false)) {
            // shouldChange auf false setzen
            intent.putExtra("shouldChange", false);
            // Werte für Änderungen speichern
            currentTaskID = intent.getStringExtra("currentTaskID");
            String textTaskName = intent.getStringExtra("textTaskName");
            String textTaskDescription = intent.getStringExtra("textTaskDescription");
            String textCategory = intent.getStringExtra("textCategory");
            // Taskliste durchgehen
            for (int i = 0; i < tasklist.size(); i++) {
                // Prüfen, ob ID übereinstimmt
                if (currentTaskID.equals(tasklist.get(i).getUniqueID())) {
                    // Task ändern
                    tasklist.get(i).changeTask(textTaskName, textTaskDescription, textCategory);
                }
            }


        }

        /*
        // Listen nach Kategorie filtern
        String filterCategory = MenuActivity.currentCategory;
        arrayAdapter.getFilter().filter(filterCategory);
        arrayAdapterClosed.getFilter().filter(filterCategory);
        */

        // ArrayAdapter aktualisieren
        arrayAdapter.notifyDataSetChanged();
        arrayAdapterClosed.notifyDataSetChanged();
        // Änderungen in SharedPrefs speichern
        saveData();
    }

    /**
     * Wird aufgerufen, bevor die Aktivität beendet wird.
     */
    @Override
    protected void onDestroy() {
        // Standardbefehl
        super.onDestroy();
        // Daten nochmal in SharedPrefs speichern (eigentlich überflüssig)
        saveData();
    }

    /**
     * Tasklist und Closed Tasklist in SharedPref speichern.
     */
    public void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences("shared_preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(tasklist);
        editor.putString("tasklist", json);
        editor.apply();

        SharedPreferences sharedPreferencesC = getSharedPreferences("shared_preferencesC", MODE_PRIVATE);
        SharedPreferences.Editor editorC = sharedPreferencesC.edit();
        Gson gsonC = new Gson();
        String jsonC = gsonC.toJson(closedTasklist);
        editorC.putString("tasklistC", jsonC);
        editorC.apply();
    }

    /**
     * Tasklist und Closed Tasklist aus SharedPref laden.
     */
    public void loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences("shared_preferences", MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("tasklist", null);
        Type type = new TypeToken<ArrayList<Task>>() {}.getType();
        tasklist = gson.fromJson(json, type);
        if (tasklist == null) {
            tasklist = new ArrayList<>();
        }

        SharedPreferences sharedPreferencesC = getSharedPreferences("shared_preferencesC", MODE_PRIVATE);
        Gson gsonC = new Gson();
        String jsonC = sharedPreferencesC.getString("tasklistC", null);
        Type typeC = new TypeToken<ArrayList<Task>>() {}.getType();
        closedTasklist = gsonC.fromJson(jsonC, typeC);
        if (closedTasklist == null) {
            closedTasklist = new ArrayList<>();
        }
    }

    /**
     * Tasklist und Closed Tasklist in SharedPref löschen (zum Zurücksetzen).
     */
    public void deleteData() {
        SharedPreferences sharedPreferences = getSharedPreferences("shared_preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();

        SharedPreferences sharedPreferencesC = getSharedPreferences("shared_preferencesC", MODE_PRIVATE);
        SharedPreferences.Editor editorC = sharedPreferencesC.edit();
        editorC.clear();
        editorC.apply();
    }
}

