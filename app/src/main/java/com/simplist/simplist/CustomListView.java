package com.simplist.simplist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

/**
 * Klasse für einen custom Array Adapter, welcher Taskliste und Checkboxen beinhaltet.
 */
public class CustomListView extends ArrayAdapter<Task> implements Filterable {
    // Taskliste beinhaltet Tasks
    private ArrayList<Task> tasklist;
    // Beinhaltet Kontext
    private Activity context;
    // Beinhaltet MainActivity, um auf Methoden der Activity zurückzugreifen
    private MainActivity mainActivity;
    // Beschreibt, um welche Tasklist es sich handelt
    private boolean isOpenTasklist;
    // Beinhaltet den Filter
    private CategorieFilter categoryFilter;
    // Beinhaltet die Liste, welche gefiltert wird
    private ArrayList<Task> filterlist;

    /**
     * Konstruktor.
     *
     * @param context
     * @param data
     * @param mainActivity
     */
    public CustomListView(@NonNull Activity context, ArrayList<Task> data, MainActivity mainActivity, boolean isOpenTasklist) {
        super(context, R.layout.custom_list_view, data);
        this.tasklist = data;
        this.context = context;
        this.mainActivity = mainActivity;
        this.isOpenTasklist = isOpenTasklist;
        this.filterlist = tasklist;
    }

    /**
     * Muss implementiert werden.
     *
     * @param position
     * @param view
     * @param parent
     * @return
     */
    public View getView(final int position, View view, ViewGroup parent) {
        // Layout Inflater
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.custom_list_view, null);

        // Position des aktuellen Task herausfinden
        Task task = tasklist.get(position);

        // GUI-Elemente initialisieren
        TextView taskTitle = rowView.findViewById(R.id.textViewRow);
        CheckBox taskState = rowView.findViewById(R.id.checkBoxRow);

        // GUI-Elemente befüllen
        taskTitle.setText(task.toString());
        taskState.setChecked(task.getTaskState());

        // Listener für Checkbox
        taskState.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                // Position des aktuellen Task herausfinden
                Task currentTask = tasklist.get(position);
                // ID des aktuellen Tasks herausfinden
                String currentTaskID = tasklist.get(position).getUniqueID();
                // Änderung des Status und wechsel in andere Taskliste
                if (isChecked) {
                    currentTask.setTaskState(true);
                    mainActivity.closedTasklist.add(currentTask);
                    mainActivity.tasklist.remove(position);
                } else {
                    currentTask.setTaskState(false);
                    mainActivity.tasklist.add(currentTask);
                    mainActivity.closedTasklist.remove(position);
                }
                // ArrayAdapter aktualisieren
                mainActivity.arrayAdapter.notifyDataSetChanged();
                mainActivity.arrayAdapterClosed.notifyDataSetChanged();
                // Änderungen in SharedPrefs speichern
                mainActivity.saveData();
            }
        });
        return rowView;
    }

    /**
     * Gibt Filter zurück.
     *
     * @return
     */
    public Filter getFilter() {
        if (categoryFilter == null) {
            categoryFilter = new CategorieFilter();
        }
        return categoryFilter;
    }

    /**
     * Filter, um nach einer Kategorie zu filtern.
     */
    private class CategorieFilter extends Filter {

        /**
         * Schreibt die Filterlogik fest.
         *
         * @param constraint
         * @return
         */
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // Filterlogik
            if (constraint != null && constraint.length() > 0 ) {
                ArrayList<Task> tempFilterlist = new ArrayList<Task>();
                for (int i = 0; i < filterlist.size(); i++) {
                    if (constraint.toString().equals(filterlist.get(i).getCategory())) {
                        tempFilterlist.add(new Task(filterlist.get(i).getTaskName(), filterlist.get(i).getTaskDescription(), filterlist.get(i).getCategory()));
                    }
                }
                results.count = tempFilterlist.size();
                results.values = tempFilterlist;
            } else {
                results.count = filterlist.size();
                results.values = filterlist;
            }
            return results;
        }

        /**
         * Andwendung des Filters.
         *
         * @param constraint
         * @param results
         */
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            tasklist = (ArrayList<Task>) results.values;
            notifyDataSetChanged();
        }
    }
}
