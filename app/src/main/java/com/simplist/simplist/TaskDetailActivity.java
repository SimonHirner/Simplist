package com.simplist.simplist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class TaskDetailActivity extends AppCompatActivity {
    // Button
    Button buttonSaveTask;
    Button buttonDeleteTask;
    // Text Views
    TextView editTextTaskNameD;
    TextView editTextTaskDescriptionD;
    // Spinner
    Spinner spinnerCategory;
    // Variablen für Textfelder
    String textTaskName;
    String textTaskDescription;
    // Variable für Spinner
    String textCategory;
    // Aktueller Task ID
    String currentTaskID;
    // Kategorieliste beinhaltet alle Kategorien
    ArrayList<String> categoryList;

    /**
     * Wird aufgerufen, wenn die Aktivität gestartet wird.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Standardbefehle
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_detail);

        // GUI-Elemente verknüpfen
        buttonSaveTask = findViewById(R.id.buttonSaveTask);
        buttonDeleteTask = findViewById(R.id.buttonDeleteTask);
        editTextTaskNameD = findViewById(R.id.editTextTaskNameD);
        editTextTaskDescriptionD = findViewById(R.id.editTextTaskDescriptionD);
        spinnerCategory = findViewById(R.id.spinnerCategoryD);

        // Kategorieliste abspeichern
        categoryList = MenuActivity.categoryList;

        // Spinner mit aktueller Kategorieliste befüllen
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, categoryList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerCategory.setAdapter(adapter);

        // GUI-Elemente befüllen
        editTextTaskNameD.setText(getIntent().getStringExtra("textTaskName"));
        editTextTaskDescriptionD.setText(getIntent().getStringExtra("textTaskDescription"));
        spinnerCategory.setSelection(MenuActivity.currentCategoryPosition);

        // Initialisierung der atuellen Task ID
        currentTaskID = getIntent().getStringExtra("currentTaskID");

        // OnClickListener für den SaveTaskButton
        buttonSaveTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Werte der Textfelder und Spinner in Variablen speichern
                textTaskName = editTextTaskNameD.getText().toString();
                textTaskDescription = editTextTaskDescriptionD.getText().toString();
                textCategory = spinnerCategory.getSelectedItem().toString();

                // Neuer Intent zur MainActivity
                Intent intent = new Intent(TaskDetailActivity.this, MainActivity.class);

                // Werte für Änderungen übergeben
                intent.putExtra("shouldChange", true);
                intent.putExtra("currentTaskID", currentTaskID);
                intent.putExtra("textTaskName", textTaskName);
                intent.putExtra("textTaskDescription", textTaskDescription);
                intent.putExtra("textCategory", textCategory);

                // Zurück zur MainActivity wechseln
                startActivity(intent);
            }

        });

        // OnClickListener für den DeleteTaskButton
        buttonDeleteTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Neuer Intent zur MainActivity
                Intent intent = new Intent(TaskDetailActivity.this, MainActivity.class);
                // shouldDel auf true setzen
                intent.putExtra("shouldDel", true);
                // Task ID übergeben
                intent.putExtra("currentTaskID", currentTaskID);
                // Zurück zur MainActivity wechseln
                startActivity(intent);
            }
        });
    }
}
