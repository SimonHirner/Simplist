package com.simplist.simplist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 *  Aktivität, um eine Kategorie zu bearbeiten.
 */
public class CategoryDetailActivity extends AppCompatActivity {
    // Button
    Button buttonSaveCategory;
    Button buttonDeleteCategory;
    // Text Views
    TextView editTextCategoryName;
    // Variable für Textfeld
    String newCategoryName;
    // Aktuelle Kategorie
    String currentCategory;

    /**
     * Wird aufgerufen, wenn die Aktivität gestartet wird.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Standardbefehle
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_detail);

        // GUI-Elemente verknüpfen
        buttonSaveCategory = findViewById(R.id.buttonSaveCategoryD);
        buttonDeleteCategory = findViewById(R.id.buttonDeleteCategoryD);
        editTextCategoryName = findViewById(R.id.editTextCategoryNameD);

        // GUI-Elemente befüllen
        editTextCategoryName.setText(getIntent().getStringExtra("textCategoryName"));

        // Aktuelle Kategorie speichern
        currentCategory = getIntent().getStringExtra("textCategoryName");

        // OnClickListener für den SaveCategoryButton
        buttonSaveCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Werte des Textfeldes in Variable speichern
                newCategoryName = editTextCategoryName.getText().toString();
                // Neuer Intent zur MainActivity
                Intent intent = new Intent(CategoryDetailActivity.this, MenuActivity.class);
                // Werte für Änderungen übergeben
                intent.putExtra("shouldChangeCategory", true);
                intent.putExtra("newCategoryName", newCategoryName);
                intent.putExtra("oldCategoryName", currentCategory);
                // Zurück zur MenuActivity wechseln
                startActivity(intent);
            }

        });

        // OnClickListener für den DeleteCategoryButton
        buttonDeleteCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Neuer Intent zur MenuActivity
                Intent intent = new Intent(CategoryDetailActivity.this, MenuActivity.class);
                // shouldDelCategory auf true setzen
                intent.putExtra("shouldDelCategory", true);
                // Kategorie übergeben
                intent.putExtra("delCategory", currentCategory);
                // Zurück zur MenuActivity wechseln
                startActivity(intent);
            }
        });
    }
}
