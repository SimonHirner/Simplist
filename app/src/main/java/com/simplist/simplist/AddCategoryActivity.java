package com.simplist.simplist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Aktivität, um eine neue Kategorie hinzuzufügen.
 */
public class AddCategoryActivity extends AppCompatActivity {
    // Button
    Button buttonAddCategory;
    // Text View
    TextView editTextCategoryName;
    // Variable für Text View
    String categoryName;

    /**
     * Wird aufgerufen, wenn die Aktivität gestartet wird.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Standardbefehle
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);

        // GUI-Elemente verknüpfen
        buttonAddCategory = findViewById(R.id.buttonAddCategory);
        editTextCategoryName = findViewById(R.id.editTextCategoryName);

        // OnClickListener für den AddTaskButton
        buttonAddCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Werte des Textfeldes in Variablen speichern
                categoryName = editTextCategoryName.getText().toString();
                // Neuer Intent zur MenuActivity
                Intent intent = new Intent(AddCategoryActivity.this, MenuActivity.class);
                // shouldAddCategory auf true setzen
                intent.putExtra("shouldAddCategory", true);
                // Neue Kategorie übergeben
                intent.putExtra("newCategory", categoryName);
                // Zurück zur MainActivity wechseln
                startActivity(intent);
            }

        });


    }
}
